# Softplan Interview Test - Ex. 2
### Avaliação e refactoring de código


Utilizarei as linhas do código original como base para listar os problemas.

1. Linha 8:
    1. Qualquer membro de classe deve ser `private`, a não ser que seja estritamente necessário o contrário.
    2. Strings definidas diretamente no código, além de não permitirem i18n, após a compilação são substituidas diretamente no pool de strings de cada classe que a utiliza.
       Dessa forma mesmo que o método que chama a variável não seja utilizado, esse valor será carregado na memória assim que a classe for carregada.
    3. um `typo` no nome da variável: correto seria `umaNota` ao invés de `umoNota`

2. Linha 10:
    1. Mesmo problema de acesso da anterior.
    2. Não é necessária a existência dessa variável, pois cada vez que o método `geraObservacao` é chamado ela é modificada localmente, sem impactos externos ao método.

3. Linha 12: o comentário sobre o método público deveria ser feito com `javadoc` para facilitar o uso dos desenvolvedores.
   Várias IDEs mostram esse tipo de documentação.
    
4. Falta especificação de tipo nas listas nas linhas 13 e 22. (`List<Integer>`)

5. Linha 15 no método `geraObservacao`, `NullPointerException` caso o parâmetro seja `null`

6. Linha 16: concatenação de string errada. Deveria estar junto com a lógica de montagem do texto no método `retornaCodigos`.

7. Especificar `final` em todas as variáveis que não sofrem atribuições. Esse tipo de prática facilita leitura e manutenção do código.
   Assim, uma variável que será modificada naquele escopo, se destacará em relação as outras, gerando um ponto de interesse para o desenvolvedor.
   Além disso, pode ajudar o compilador no seu processo de geração/otimização de código.

8. No método `retornaCodigos` está utilizando várias vezes concatenação de strings. 
   Para esse tipo de operação deve-se utilizar `StringBuilder` e `StringBuffer`(apenas nos casos de concorrência).
   Concatenação de strings é pesado e gera várias strings no pool que não serão removidas da memória.
   
9. Linha 23:
    1. no método `retornaCodigos`, como não tem a checagem no método `geraObservacao`, ocorrerá `NullPointerException` caso o parâmetro seja `null`.
    2. Seria mais interssante fazer a comparação como sendo `> 1` para ficar mais claro a diferenciação de exatamente 1 ou mais de 1. 

10. Linha 24: extrair string para uma constante, ou message bundle para i18n.

11. Linha 33: inicialização da string é desnecessária, dado que a lógica logo abaixo inicializa em todas as situações.

12. Linha 34:
    1. `cod.toString() == null` é sempre falso. 
    Apesar de ser possível alguém sobescrever o método `toString()` de suas classes e retornarem `null`,
    É algo completamente contra a especificação do java, e a checagem é completamente desnecessária, pois o erro estaria no método `toString()` que retorna `null`.
    2. Nunca se deve utilizar o `cod.toString().length()` para comparar o tamanho de um `StringBuilder`.
    O correto seria chamar `cod.length()`.
    3. Não existe tamanho negativo, logo fica mais claro `cod.length() == 0`.
    
13. Linha 41: concatenação de strings na chamada `cod.append(s + c)`. 
    O correto seria: 
    ```java
    cod.append(s);
    cod.append(c);
    ``` 
14. Linha 44: concatenação de strings. 
    Alterando um pouco a lógica, o `StringBuilder` poderia ser inicializado com o valor de `texto`, evitando assim esse tipo de operação.

15. Não tem um padrão de codificação bem definido.
    1. os `if`s hora tem bloco definido (linha 23), hora não (linha 34)
    2. hora usa constante (linha 26), hora não (linha 24)
    3. O uso de `Iterator` no for está correto mas não é habitual, o mais comum é `for` com índices ou `foreach`
    
