import java.util.List;

public class GeradorObservacao {

    // Textos pré-definidos
    // deixei como constantes mesmo para não adicionar
    // a complicação de ler um ResourceBundle com as mensagens
    private static final String umaNota = "Fatura da nota fiscal de simples remessa: ";
    private static final String variasNotas = "Fatura das notas fiscais de simples remessa: ";

    /**
     * Gera observações, com texto pre-definido, incluindo os números, das notas fiscais, recebidos no parâmetro
     * @param lista dos números das notas
     *
     * @return observação gerada
     */
    public String geraObservacao(final List<Integer> lista) {
        if (lista == null || lista.isEmpty()) return "";

        return retornaCodigos(lista);
    }

    // Cria observação
    private String retornaCodigos(final List<Integer> lista) {
        final StringBuilder obs = new StringBuilder();
        if (lista.size() > 1) {
            obs.append(variasNotas);
        } else {
            obs.append(umaNota);
        }

        assert lista.size() > 0;
        obs.append(lista.get(0));

        for (int i = 1; i < lista.size(); i++) {
            final int restante = lista.size() - i;
            if (restante > 1) {
                obs.append(',');
            } else if (restante == 1){
                obs.append(' ');
                obs.append('e');
            }
            obs.append(' ');
            obs.append(lista.get(i));
        }
        obs.append('.');

        return obs.toString();
    }
}